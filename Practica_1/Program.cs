﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Practica_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //ejercicio1();
            //ejercicio2();
            //ejercicio3();
            //ejercicio5();
            ejercicio6();
        }
        public static void ejercicio1()
        {
            Console.WriteLine("Introduce 10 calificaciones");
            int num = 0;
            double nota;
            int aprobados = 0;
            int sobresaliente = 0;
            while (num < 10)
            {
                nota = Double.Parse(Console.ReadLine());
                if (nota >= 5)
                {
                    aprobados = aprobados + 1;
                }
                if (nota >= 9)
                {
                    sobresaliente = sobresaliente + 1;
                }
                num = num + 1;
            }
            Console.WriteLine("Los aprobados son: " + aprobados);
            Console.WriteLine("Los sobresalientes son: " + sobresaliente);
        }
        public static void ejercicio2()
        {
            Console.WriteLine("Introduce la fecha de recepción");
            int dia, mes, año;
            dia = Int32.Parse(Console.ReadLine());
            mes = Int32.Parse(Console.ReadLine());
            año = Int32.Parse(Console.ReadLine());

            if (dia < 1|| dia > 30 || mes < 1 || mes > 12)
            {
                Console.WriteLine("Algo has hecho mal");
            }
            else
            {
                mes = mes + 3;

                if (mes>12)
                {
                    año = año + 1;
                    mes = mes - 12;
                }
            }
            Console.WriteLine("La fecha de pago es "+ dia +"/"+ mes +"/"+ año);
        }
        public static void ejercicio3()
        {
            int i = 0;
            int[] numeros = new int[10];
            numeros[0] = 27;
            numeros[1] = 30;
            numeros[2] = 4;
            numeros[3] = 9;
            numeros[4] = 53;
            numeros[5] = 45;
            numeros[6] = 11;
            numeros[7] = 17;
            numeros[8] = 13;
            numeros[9] = 63;
            while (i<10)
            {
                Console.WriteLine(numeros[i]);
                i++;
            }
            i = 9;
            while (i > -1)
            {
                Console.WriteLine(numeros[i]);
                i--;
            }
        }

        public static void ejercicio5()
        {
            int num1 = 0;
            int num2 = 0;
            int i = 0;
            int z = 0;
            int[] numeros = new int[10];
            numeros[0] = Int32.Parse(Console.ReadLine());
            numeros[1] = Int32.Parse(Console.ReadLine());
            numeros[2] = Int32.Parse(Console.ReadLine());
            numeros[3] = Int32.Parse(Console.ReadLine());
            numeros[4] = Int32.Parse(Console.ReadLine());
            numeros[5] = Int32.Parse(Console.ReadLine());
            numeros[6] = Int32.Parse(Console.ReadLine());
            numeros[7] = Int32.Parse(Console.ReadLine());
            numeros[8] = Int32.Parse(Console.ReadLine());
            numeros[9] = Int32.Parse(Console.ReadLine());
            while (i<10)
            {
                if (numeros[i]>num1)
                {
                    num1 = numeros[i];
                    z = i;
                }
                i++;
            }
            Array.Clear(numeros, z, 1);
            i = 0;
            while (i<10)
            {
                if (numeros[i] > num2)
                {
                    num2 = numeros[i];
                }
                i++;
            }
            Console.WriteLine("Los números más altos son: "+num1 + " " + num2);
        }
        public static void ejercicio6()
        {
            int[] numeros = new int[10];
            numeros[0] = Int32.Parse(Console.ReadLine());
            numeros[1] = Int32.Parse(Console.ReadLine());
            numeros[2] = Int32.Parse(Console.ReadLine());
            numeros[3] = Int32.Parse(Console.ReadLine());
            numeros[4] = Int32.Parse(Console.ReadLine());
            numeros[5] = Int32.Parse(Console.ReadLine());
            numeros[6] = Int32.Parse(Console.ReadLine());
            numeros[7] = Int32.Parse(Console.ReadLine());
            numeros[8] = Int32.Parse(Console.ReadLine());
            numeros[9] = Int32.Parse(Console.ReadLine());

            int[] numeros2 = numeros.Distinct().ToArray();

            Console.WriteLine("En estos diez números hay "+ numeros2.Length + " valores diferentes");
        }
    }
}
